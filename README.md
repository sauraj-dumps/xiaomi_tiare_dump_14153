## tiare-user 8.1.0 OPM1.171019.026 V10.2.21.0.OCLRUXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: tiare
- Brand: Xiaomi
- Flavor: tiare-user
- Release Version: 8.1.0
- Kernel Version: 3.18.71
- Id: OPM1.171019.026
- Incremental: V10.2.21.0.OCLRUXM
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: ru-RU
- Screen Density: undefined
- Fingerprint: Xiaomi/tiare_ru/tiare:8.1.0/OPM1.171019.026/V10.2.21.0.OCLRUXM:user/release-keys
- OTA version: 
- Branch: tiare-user-8.1.0-OPM1.171019.026-V10.2.21.0.OCLRUXM-release-keys
- Repo: xiaomi_tiare_dump_14153
